package com.pedro.compents;

import android.graphics.Bitmap;

/**
 * Created by Pedro on 6/6/14.
 */
public interface AsyncImageViewListener {
    void finished(AsyncImageView imageView, Bitmap image);
    void error(AsyncImageView imageView, String message);
}
