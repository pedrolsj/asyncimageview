package com.pedro.compents;

/**
 * Created by pedro on 11/06/14.
 */
public class AsyncConfiguration implements Cloneable {

    long expiration; //Used when use CACHE_MODE_FILE OR CACHE_MODE_RAM

    String savePath;
    String downloadURL;

    boolean ignoreSaveIfCanceled;
    boolean useMemoryCacheInFileIfAvailable;
    boolean loadFileInMemoryCache;
    int cacheMode;

    AsyncConfigurationListener listener;

    final static int CACHE_MODE_NO_CACHE = 1;
    final static int CACHE_MODE_RAM = 2;
    final static int CACHE_MODE_FILE = 3;

    public AsyncConfiguration(String downloadURL) {
        init();
        this.downloadURL = downloadURL;
    }

    void init() {
        this.cacheMode = CACHE_MODE_NO_CACHE;
        this.ignoreSaveIfCanceled = false;
        this.useMemoryCacheInFileIfAvailable = false;
        this.loadFileInMemoryCache = false;
        this.listener = null;
        this.savePath = null;
        this.expiration = 0;
    }

    public boolean isLoadFileInMemoryCache() {
        return loadFileInMemoryCache;
    }

    public void setLoadFileInMemoryCache(boolean loadFileInMemoryCache) {
        this.loadFileInMemoryCache = loadFileInMemoryCache;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    public String getSavePath() {
        return savePath;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public AsyncConfigurationListener getListener() {
        return listener;
    }

    public void setListener(AsyncConfigurationListener listener) {
        this.listener = listener;
    }

    public boolean isIgnoreSaveIfCanceled() {
        return ignoreSaveIfCanceled;
    }

    public void setIgnoreSaveIfCanceled(boolean ignoreSaveIfCanceled) {
        this.ignoreSaveIfCanceled = ignoreSaveIfCanceled;
    }

    public boolean isUseMemoryCacheInFileIfAvailable() {
        return useMemoryCacheInFileIfAvailable;
    }

    public void setUseMemoryCacheInFileIfAvailable(boolean useMemoryCacheInFileIfAvailable) {
        this.useMemoryCacheInFileIfAvailable = useMemoryCacheInFileIfAvailable;
    }

    public int getCacheMode() {
        return cacheMode;
    }

    public void setCacheMode(int cacheMode) {
        this.cacheMode = cacheMode;
    }

    @Override
    public AsyncConfiguration clone() {

        AsyncConfiguration clone = new AsyncConfiguration(this.downloadURL);
        clone.setCacheMode(this.cacheMode);
        clone.setIgnoreSaveIfCanceled(this.ignoreSaveIfCanceled);
        clone.setListener(this.listener);
        clone.setSavePath(this.savePath);
        clone.setExpiration(this.expiration);
        clone.setUseMemoryCacheInFileIfAvailable(this.useMemoryCacheInFileIfAvailable);

        return clone;

    }

}
