package com.pedro.compents;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.digisa.ipubreview.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by pedro on 11/06/14.
 */
public class AsyncDownloadManager {

    static AsyncDownloadManager instance;
    final String TAG = "AsyncImageViewManager";

    HashMap<String, ArrayList<AsyncConfiguration>> map;
    HashMap<String, Bitmap> memoryCache;
    ArrayList<String> order;

    AsyncImageViewManagerTask currentTask;

    public AsyncDownloadManager() {
        this.map = new HashMap<String, ArrayList<AsyncConfiguration>>();
        this.memoryCache = new HashMap<String, Bitmap>();
        this.order = new ArrayList<String>();
        this.currentTask = null;
    }

    public void add(AsyncConfiguration configuration) {

        ArrayList<AsyncConfiguration> list = this.map.get(configuration.getDownloadURL());

        if(list == null) {
            list = new ArrayList<AsyncConfiguration>();
        }

        for (AsyncConfiguration i : list) {
            if(i.listener == configuration.listener) {
                list.remove(i);
                break;
            }
        }

        list.add(configuration.clone());

        if(!this.order.contains(configuration.getDownloadURL())) {
            this.order.add(configuration.getDownloadURL());
        }

        this.map.put(configuration.getDownloadURL(), list);

        startNext();

    }

    public void remove(AsyncConfiguration configuration) {
        this.remove(configuration.getDownloadURL(), configuration.getListener());
    }

    public void remove(String urlDownload, AsyncConfigurationListener listener) {

        ArrayList<AsyncConfiguration> list = this.map.get(urlDownload);

        if (list == null) return;

        for (AsyncConfiguration i : list) {

            if(i.listener == listener) {

                if(i.ignoreSaveIfCanceled) {
                    list.remove(i);
                }else{
                    i.listener = null;
                }

                break;
            }

        }

        if(list.size() == 0) {

            this.map.remove(urlDownload);
            this.order.remove(urlDownload);

            if(this.currentTask != null && this.currentTask.urlString.equals(urlDownload)) {

                this.currentTask.cancel(true);
                this.currentTask = null;

                startNext();

            }

        }

    }

    public boolean removeMemoryCache(String urlString) {
        return this.memoryCache.remove(urlString) != null;
    }

    public void clearMemoryCache() {
        this.memoryCache.clear();
    }

    public static AsyncDownloadManager getInstance() {

        if(instance == null) {
            instance = new AsyncDownloadManager();
        }

        return instance;

    }

    class AsyncImageViewManagerTask extends AsyncTask<ArrayList<AsyncConfiguration>, Integer, Bitmap> {

        String urlString;
        ArrayList<AsyncConfiguration> configurations;
        String errorMessage;
        boolean completed;

        public AsyncImageViewManagerTask(ArrayList<AsyncConfiguration> configurations) {

            this.configurations = configurations;

            AsyncConfiguration configuration = this.configurations.get(0);

            this.urlString = configuration.getDownloadURL();
            this.completed = false;

        }

        @Override
        protected Bitmap doInBackground(ArrayList<AsyncConfiguration>[] objects) {

            AsyncConfiguration configuration = this.configurations.get(0);

            try {

                InputStream is = AsyncUtil.download(urlString);

                if(this.isCancelled()) return null;

                if(is != null) {

                    Bitmap bitmap = BitmapFactory.decodeStream(is);

                    if(bitmap != null) {

                        boolean isWrote = false;

                        for (AsyncConfiguration i : configurations) {

                            if(i.cacheMode == AsyncConfiguration.CACHE_MODE_RAM) {
                                AsyncDownloadManager.this.memoryCache.put(urlString, bitmap);
                            }else if(i.cacheMode == AsyncConfiguration.CACHE_MODE_FILE){

                                if(i.loadFileInMemoryCache) {
                                    AsyncDownloadManager.this.memoryCache.put(urlString, bitmap);
                                }

                                if(isWrote) continue;

                                isWrote = true;

                                if(configuration.savePath == null) {
                                    this.errorMessage = "O caminho local do arquivo é inválido";
                                    return null;
                                }

                                File file = new File(configuration.savePath);
                                file.setLastModified(new Date().getTime());

                                String ext = StringUtils.getExtension(configuration.savePath);
                                FileOutputStream fOut;

                                if(file.exists()) {
                                    file.delete();
                                }

                                try {
                                    fOut = new FileOutputStream(file);
                                }catch (FileNotFoundException e) {
                                    this.errorMessage = e.getMessage();
                                    return null;
                                }

                                boolean result;

                                if(ext == null || ext.equals("png") || !(ext.equals("jpg") || ext.equals("jpeg"))) {
                                    result = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                                }else{
                                    result = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                                }

                                if(!result) {
                                    this.errorMessage = "Houve um erro desconhecido ao tentar salvar o arquivo no disco";
                                    return null;
                                }

                            }

                        }

                        return bitmap;

                    }else{
                        this.errorMessage = "A imagem é inválida";
                    }

                }else{
                    this.errorMessage = "O resposta do servidor é inválida";
                }

            } catch (IOException e) {
                this.errorMessage = e.getMessage();
            }

            this.errorMessage = "Houve um erro desconhecido na operação";

            return null;

        }

        @Override
        protected void onPostExecute(Bitmap result) {

            this.completed = true;

            if(this.isCancelled()) return;

            if(result == null) {
                AsyncDownloadManager.this.notifyError(this.errorMessage, this);
            }else{
                AsyncDownloadManager.this.notifySuccess(result, this);
            }

        }

    }

    void notifyError(String message, AsyncImageViewManagerTask task) {

        for (AsyncConfiguration i : task.configurations) {

            if(i.listener != null) {
                i.listener.error(message);
            }

        }

        this.map.remove(task.urlString);
        this.order.remove(task.urlString);

        startNext();

    }

    void notifySuccess(Bitmap result, AsyncImageViewManagerTask task) {

        for (AsyncConfiguration i : task.configurations) {

            if(i.listener != null) {
                i.listener.sucess(result);
            }

        }

        this.map.remove(task.urlString);
        this.order.remove(task.urlString);

        startNext();

    }

    void startNext() {

        if(this.order.size() == 0 || (this.currentTask != null && !this.currentTask.completed)) return;

        /* If used mode CACHE_MODE_FILE and the file stay in memoryCache, just write to file next notify the listener */

        String key = this.order.get(0);
        ArrayList<AsyncConfiguration> configurations = this.map.get(key);
        int totalListeners = configurations.size();

        for (int i=0;i<totalListeners;i++) {

            AsyncConfiguration configuration = configurations.get(i);

            if(configuration.cacheMode == AsyncConfiguration.CACHE_MODE_RAM) {

                Bitmap cache = this.memoryCache.get(configuration.getDownloadURL());

                if(cache != null) {

                    if(configuration.listener != null) {
                        configuration.listener.sucess(cache);
                    }

                    configurations.remove(i);
                    totalListeners--;
                    i--;

                }

            }else if(configuration.cacheMode == AsyncConfiguration.CACHE_MODE_FILE){

                if(configuration.savePath == null) {

                    configurations.remove(i);
                    totalListeners--;
                    i--;

                    if(configuration.listener != null) {
                        configuration.listener.error("O caminho local do arquivo é inválido");
                    }

                    continue;

                }

                File file = new File(configuration.savePath);
                Bitmap cacheFromMemory = this.memoryCache.get(configuration.getDownloadURL());

                if(cacheFromMemory != null && configuration.useMemoryCacheInFileIfAvailable) {

                    /* Feito essa intervenção para evitar processamento desnecessário quando está usando ListView */

                    if(configuration.listener == null) {
                        configurations.remove(i);
                        totalListeners--;
                        i--;
                        continue;
                    }

                    /*
                    boolean writeTofile = false;

                    if(file.exists()) {

                        // Se o arquivo estiver expirado, atualiza

                        if (configuration.expiration != 0 && file.lastModified() + configuration.expiration <= new Date().getTime()) {
                            writeTofile = true;
                        }

                    }else{

                        // Se o arquivo não existir, atualiza

                        writeTofile = true;
                    }

                    if(writeTofile) {

                        String ext = StringUtils.getExtension(configuration.savePath);
                        FileOutputStream fOut;

                        try {
                            fOut = new FileOutputStream(file);
                        }catch (FileNotFoundException e) {

                            configurations.remove(i);
                            totalListeners--;
                            i--;

                            if(configuration.listener != null) {
                                configuration.listener.error(e.getMessage());
                            }

                            continue;

                        }

                        file.setLastModified(new Date().getTime());

                        // Se precisar envia mensagem de sucesso ou erro em caso de retorno falso

                        if(ext == null || ext.equals("png") || !(ext.equals("jpg") || ext.equals("jpeg"))) {
                            cacheFromMemory.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                        }else{
                            cacheFromMemory.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                        }

                    }
                    */

                    configurations.remove(i);
                    totalListeners--;
                    i--;

                    if(configuration.listener != null) {
                        configuration.listener.sucess(cacheFromMemory);
                    }

                }else if(file.exists()) {

                    /* Feito essa intervenção para evitar processamento desnecessário quando está usando ListView */

                    if(configuration.listener == null) {
                        configurations.remove(i);
                        totalListeners--;
                        i--;
                        continue;
                    }

                    if(configuration.expiration == 0 || file.lastModified() + configuration.expiration > new Date().getTime()) {

                        Bitmap cache = BitmapFactory.decodeFile(configuration.savePath);

                        if(cache != null) {

                            if(configuration.isLoadFileInMemoryCache()) {
                                this.memoryCache.put(configuration.getDownloadURL(), cache);
                            }

                            configurations.remove(i);
                            totalListeners--;
                            i--;

                            if (configuration.listener != null) {
                                configuration.listener.sucess(cache);
                            }

                        }

                    }

                }

            }

        }

        if(configurations.size() == 0) {

            this.map.remove(key);
            this.order.remove(key);

            startNext();

        }else{
            this.currentTask = new AsyncImageViewManagerTask(configurations);
            this.currentTask.execute();
        }

    }

}
