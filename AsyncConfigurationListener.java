package com.pedro.compents;

import android.graphics.Bitmap;

/**
 * Created by pedro on 11/06/14.
 */
public interface AsyncConfigurationListener {

    void sucess(Bitmap image);
    void error(String message);

}
