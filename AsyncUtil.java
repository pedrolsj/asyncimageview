package com.pedro.compents;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by pedro on 11/06/14.
 */
public class AsyncUtil {

    public static InputStream download(String urlString) throws IOException {

        URL url = new URL(urlString);

        return url.openStream();

    }

}
