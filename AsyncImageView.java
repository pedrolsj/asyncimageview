package com.pedro.compents;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.digisa.ipubreview.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by pedro on 05/06/14.
 */
public class AsyncImageView extends ImageView implements AsyncConfigurationListener {

    private AsyncConfiguration asyncConfiguration;
    private boolean ignoreDownloadIfCanceled;
    private boolean useMemoryCacheInFileIfAvailable;
    private boolean loadFileInMemoryCache;
    private AsyncImageViewListener asyncImageViewListener;

    public AsyncImageView(android.content.Context context) {
        super(context);
        init(context);
    }

    public AsyncImageView(Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AsyncImageView(Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public boolean isIgnoreDownloadIfCanceled() {
        return ignoreDownloadIfCanceled;
    }

    public void setIgnoreDownloadIfCanceled(boolean ignoreDownloadIfCanceled) {
        this.ignoreDownloadIfCanceled = ignoreDownloadIfCanceled;
    }

    public boolean isUseMemoryCacheInFileIfAvailable() {
        return useMemoryCacheInFileIfAvailable;
    }

    public void setUseMemoryCacheInFileIfAvailable(boolean useMemoryCacheInFileIfAvailable) {
        this.useMemoryCacheInFileIfAvailable = useMemoryCacheInFileIfAvailable;
    }

    public AsyncImageViewListener getAsyncImageViewListener() {
        return asyncImageViewListener;
    }

    public void setAsyncImageViewListener(AsyncImageViewListener asyncImageViewListener) {
        this.asyncImageViewListener = asyncImageViewListener;
    }

    /**
     * Realiza o download assíncrono, porém não salva no disco ou RAM
     * O mesmo que setAsyncImage(url, false)
     *
     * @param url URL para download da imagem
     *
     * @author  Pedro Lucas
     * @version 0.1
     * @since   2014-06-11
     */
    public void setAsyncImage(String url) {
        setAsyncImage(url, false);
    }

    /**
     * Realiza o download assíncrono, e pode salvar na RAM ou não
     *
     * @param url URL para download da imagem
     * @param memoryCache se verdadeiro, salva a imagem em disco
     *
     * @author  Pedro Lucas
     * @version 0.1
     * @since   2014-06-11
     */
    public void setAsyncImage(String url, boolean memoryCache) {

        this.asyncConfiguration = new AsyncConfiguration(url);
        this.asyncConfiguration.setListener(this);
        this.asyncConfiguration.setIgnoreSaveIfCanceled(this.ignoreDownloadIfCanceled);
        this.asyncConfiguration.setUseMemoryCacheInFileIfAvailable(this.useMemoryCacheInFileIfAvailable);

        if(memoryCache) {
            this.asyncConfiguration.setCacheMode(AsyncConfiguration.CACHE_MODE_RAM);
        }

        AsyncDownloadManager.getInstance().add(this.asyncConfiguration);

    }

    /**
     * Realiza o download assíncrono, e salva o arquivo no disco, porém não expira e nem aloca na RAM
     * O mesmo que setAsyncImage(url, savePath, 0)
     *
     * @param url URL para download da imagem
     * @param savePath Path local onde irá ser salvo o arquivo, se já existir, remove e adiciona o novo arquivo
     *
     * @author  Pedro Lucas
     * @version 0.1
     * @since   2014-06-11
     */
    public void setAsyncImage(String url, String savePath) {
        setAsyncImage(url, savePath, 0);
    }

    /**
     * Realiza o download assíncrono, e salva o arquivo no disco, com tempo de expiração, porém não aloca na RAM
     * O mesmo que setAsyncImage(url, savePath, expiration, false)
     *
     * @param url URL para download da imagem
     * @param savePath Path local onde irá ser salvo o arquivo, se já existir, remove e adiciona o novo arquivo
     * @param expiration Tempo de expiração do arquivo no disco em milissegundos
     *
     * @author  Pedro Lucas
     * @version 0.1
     * @since   2014-06-11
     */
    public void setAsyncImage(String url, String savePath, long expiration) {

        this.asyncConfiguration = new AsyncConfiguration(url);
        this.asyncConfiguration.setListener(this);
        this.asyncConfiguration.setIgnoreSaveIfCanceled(this.ignoreDownloadIfCanceled);
        this.asyncConfiguration.setUseMemoryCacheInFileIfAvailable(this.useMemoryCacheInFileIfAvailable);
        this.asyncConfiguration.setLoadFileInMemoryCache(this.loadFileInMemoryCache);
        this.asyncConfiguration.setCacheMode(AsyncConfiguration.CACHE_MODE_FILE);
        this.asyncConfiguration.setSavePath(savePath);
        this.asyncConfiguration.setExpiration(expiration);

        AsyncDownloadManager.getInstance().add(this.asyncConfiguration);

    }

    /**
     * Cancela o download da imagem atual
     *
     * @author  Pedro Lucas
     * @version 0.1
     * @since   2014-06-11
     */
    public void cancelAsyncImage() {
        if(this.asyncConfiguration != null) {
            AsyncDownloadManager.getInstance().remove(this.asyncConfiguration);
        }
    }

    /**
     * Recupera a versão clonada do objeto de configuração
     *
     * @author  Pedro Lucas
     * @version 0.1
     * @since   2014-06-11
     */
    public AsyncConfiguration getClonedConfiguration() {
        return this.asyncConfiguration.clone();
    }

    void init(Context context) {
        this.ignoreDownloadIfCanceled = false;
        this.useMemoryCacheInFileIfAvailable = true;
        this.loadFileInMemoryCache = true;
    }

    @Override
    public void sucess(Bitmap image) {
        if(this.asyncImageViewListener != null) {
            this.asyncImageViewListener.finished(this, image);
        }
        setImageBitmap(image);
    }

    @Override
    public void error(String message) {
        if(this.asyncImageViewListener != null) {
            this.asyncImageViewListener.error(this, message);
        }
    }

    public boolean isLoadFileInMemoryCache() {
        return loadFileInMemoryCache;
    }

    public void setLoadFileInMemoryCache(boolean loadFileInMemoryCache) {
        this.loadFileInMemoryCache = loadFileInMemoryCache;
    }
}